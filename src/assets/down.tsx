import * as React from "react";

function DownIcon(props: any) {
  return (
    <svg
      width={10}
      height={7}
      viewBox="0 0 10 7"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M5 6.835l-5-5L1.167.668 5 4.501 8.833.668 10 1.835l-5 5z"
        fill="#000"
      />
    </svg>
  );
}

export default DownIcon;
