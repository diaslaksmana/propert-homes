import * as React from "react";

function Facebook(props: any) {
  return (
    <svg
      width={10}
      height={15}
      viewBox="0 0 10 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M3.002 14.393l-.021-6.297H.219V5.397H2.98V3.598C2.981 1.17 4.521 0 6.737 0 7.8 0 8.712.077 8.977.112v2.537L7.44 2.65c-1.206 0-1.439.56-1.439 1.381v1.366h3.425l-.92 2.7H6v6.296H3.002z"
        fill="#2C373E"
      />
    </svg>
  );
}

export default Facebook;
