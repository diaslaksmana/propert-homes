import * as React from "react";

function Instagram(props: any) {
  return (
    <svg
      width={20}
      height={18}
      viewBox="0 0 20 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g clipPath="url(#clip0_776_44)">
        <path
          d="M13.648 1.5H5.914c-2.136 0-3.867 1.679-3.867 3.75v7.5c0 2.071 1.731 3.75 3.867 3.75h7.734c2.136 0 3.868-1.679 3.868-3.75v-7.5c0-2.071-1.732-3.75-3.868-3.75z"
          stroke="#fff"
          strokeWidth={2}
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M12.874 8.529a2.921 2.921 0 01-.314 1.822 3.05 3.05 0 01-1.356 1.297 3.18 3.18 0 01-1.882.283 3.129 3.129 0 01-1.696-.84 2.97 2.97 0 01-.867-1.646A2.92 2.92 0 017.05 7.62a3.047 3.047 0 011.339-1.314A3.178 3.178 0 0110.267 6a3.13 3.13 0 011.734.846c.47.456.775 1.045.873 1.682z"
          stroke="#fff"
          strokeWidth={2}
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <ellipse cx={14.0352} cy={4.875} rx={1.16016} ry={1.125} fill="#fff" />
      </g>
      <defs>
        <clipPath id="clip0_776_44">
          <path fill="#fff" transform="translate(.5)" d="M0 0H18.5625V18H0z" />
        </clipPath>
      </defs>
    </svg>
  );
}

export default Instagram;
