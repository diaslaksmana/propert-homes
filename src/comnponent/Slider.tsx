"use client";
import React, { useState } from "react";
import Image from "next/image";
import slide1 from "../assets/pexels-photo-1396132.jpeg";
import slide2 from "../assets/pexels-photo-186077.jpeg";
import slide3 from "../assets/pexels-photo-259588.jpeg";
const Slider = () => {
  const [currentSlide, setCurrentSlide] = useState(0);

  const prevSlide = () => {
    setCurrentSlide((prev) => (prev === 0 ? 2 : prev - 1));
  };

  const nextSlide = () => {
    setCurrentSlide((prev) => (prev === 2 ? 0 : prev + 1));
  };
  return (
    <div className="carousel relative">
      <ul className="w-full">
        <li className={`slide ${currentSlide === 0 ? "active" : ""}`}>
          <div className="card">
            <Image
              src={slide1}
              alt="slide1"
              className="w-full object-cover object-center h-[70vh]"
            />
          </div>
        </li>
        <li className={`slide ${currentSlide === 1 ? "active" : ""}`}>
          <div className="card">
            <Image
              src={slide2}
              alt="slide2"
              className="w-full object-cover object-center h-[70vh]"
            />
          </div>
        </li>
        <li className={`slide ${currentSlide === 2 ? "active" : ""}`}>
          <div className="card">
            <Image
              src={slide3}
              alt="slide3"
              className="w-full object-cover object-center h-[70vh]"
            />
          </div>
        </li>
      </ul>
      <div className="navigate absolute top-[50%] flex justify-between items-center z-30 w-full">
        <button
          className="prev ml-[60px] border-primary border-[1px] bg-[#fff] h-[30px] w-[30px] grid place-content-center"
          onClick={prevSlide}
        >
          &#10094;
        </button>
        <button
          className="next mr-[60px]  border-primary border-[1px] bg-[#fff] h-[30px] w-[30px] grid place-content-center"
          onClick={nextSlide}
        >
          &#10095;
        </button>
      </div>
    </div>
  );
};

export default Slider;
