"use client";
import React, { useEffect, useState } from "react";
import Logo from "../assets/Logo.png";
import Image from "next/image";
import DownIcon from "@/assets/down";
import Humberger from "@/assets/Humberger";
const Header = () => {
  const [isActive, setIsActive] = useState(false);

  const toggleBodyClass = () => {
    setIsActive((prev) => !prev);
  };

  useEffect(() => {
    if (isActive) {
      document.body.classList.add("active");
    } else {
      document.body.classList.remove("active");
    }
  }, [isActive]);
  return (
    <header className="fixed w-full bg-[#fff] top-0 left-0 z-[9999]">
      <div className="container mx-auto px-5 py-5">
        <nav className="flex justify-between items-center">
          <div className="logo">
            <Image
              src={Logo}
              alt="logo"
              className="md:max-w-[200px] max-w-[100px]"
            />
          </div>
          <div className="nav-menu">
            <ul className="flex justify-between gap-5 items-center">
              <li>
                <button className="flex items-center gap-2 font-semibold">
                  Tentang Kami <DownIcon />
                </button>
              </li>
              <li>
                <button className="flex items-center gap-2 font-semibold active-nav">
                  Produk & Layanan <DownIcon />
                </button>
              </li>
              <li>
                <a href="" className="font-semibold">
                  Blog
                </a>
              </li>
              <li>
                <a href="" className="font-semibold">
                  FAQ
                </a>
              </li>
            </ul>
          </div>
          <div className="nav-login flex gap-5 items-center">
            <a href="" className="font-semibold">
              Daftar
            </a>
            <a href="" className="btn-primary">
              Masuk
            </a>
            <button className="humberger" onClick={toggleBodyClass}>
              <Humberger />
            </button>
          </div>
        </nav>
      </div>
    </header>
  );
};

export default Header;
