import React from "react";
import Image from "next/image";
import Logo from "../assets/Logo(2).png";
import Message from "../assets/Message.png";
import Instagram from "@/assets/Instagram";
import Facebook from "@/assets/Facebook";
import Youtube from "@/assets/Youtube";
import Mandiri from "@/assets/image70.png";
import Btn from "@/assets/image71.png";
import Bni from "@/assets/image72.png";
import Visa from "@/assets/image77.png";
import MasterCard from "@/assets/image78.png";
import Jcb from "@/assets/image79.png";
import Asco from "@/assets/asco1.png";
import Ineco from "@/assets/image82(traced).png";
import Adidaya from "@/assets/Youtube";
import Agra from "@/assets/rekan41.png";
import Sig from "@/assets/logosig.png";

const Footer = () => {
  return (
    <footer className="bg-[#012846] py-10">
      <div className="container mx-auto px-5">
        <div className="footer-top block md:flex gap-4 justify-between ">
          <div className="left basis-2/4 mb-5">
            <Image
              src={Logo}
              alt="Logo"
              className=" max-w-[169.13px]  object-cover object-center mb-4"
            />
            <p className="text-base font-normal text-[#fff] mb-4">
              SobatBangun adalah platform digital dari SIG yang bergerak dengan
              misi mengembangkan proses pembangunan dan renovasi rumah secara
              lebih baik serta berkelanjutan.
            </p>
            <h1 className="flex gap-2 text-base font-normal items-center text-[#fff] mb-4">
              <Image
                src={Message}
                alt="Message"
                width={100}
                height={100}
                className=" h-[24px] w-[24px] object-cover object-center"
              />
              <span className="underline"> sobat@sobatbangum.com</span>
            </h1>
            <h1 className="text-base font-normal text-[#fff] mb-4">
              Sosial Media :
            </h1>
            <ul className="medsos flex items-center gap-5">
              <li>
                <a href="">
                  <Instagram />
                </a>
              </li>
              <li>
                <a href="">
                  <Facebook />
                </a>
              </li>
              <li>
                <a href="">
                  <Youtube />
                </a>
              </li>
            </ul>
          </div>
          <div className="center basis-1/4 mb-5">
            <h1 className="text-xl font-bold mb-4 text-[#fff]">
              Produk & Layanan
            </h1>
            <ul className="flex flex-col gap-5">
              <li>
                <a href="" className="text-base font-normal text-[#fff]">
                  Renovasi
                </a>
              </li>
              <li>
                <a href="" className="text-base font-normal text-[#fff]">
                  Bangun Rumah
                </a>
              </li>
              <li>
                <a href="" className="text-base font-normal text-[#fff]">
                  Layanan Desain
                </a>
              </li>
              <li>
                <a href="" className="text-base font-normal text-[#fff]">
                  Teknologi Tambahan
                </a>
              </li>
              <li>
                <a href="" className="text-base font-normal text-[#fff]">
                  Beli Material
                </a>
              </li>
            </ul>
          </div>
          <div className="right basis-1/4 mb-5">
            <h1 className="text-xl font-bold mb-4 text-[#fff]">Tentang Kami</h1>
            <ul className="flex flex-col gap-5">
              <li>
                <a href="" className="text-base font-normal text-[#fff]">
                  Tentang SobatBangun
                </a>
              </li>
              <li>
                <a href="" className="text-base font-normal text-[#fff]">
                  Kebijakan Dan Privasi
                </a>
              </li>
              <li>
                <a href="" className="text-base font-normal text-[#fff]">
                  Syarat Dan Ketentuan
                </a>
              </li>
              <li>
                <a href="" className="text-base font-normal text-[#fff]">
                  FAQ
                </a>
              </li>
              <li>
                <a href="" className="text-base font-normal text-[#fff]">
                  Daftar Menjadi Mitra
                </a>
              </li>
            </ul>
          </div>
        </div>

        <div className="footer-center my-5 block md:flex  items-center justify-between gap-4">
          <div className="mb-8 md:mb-0">
            <h1 className="text-[#fff] font-semibold text-base">
              Kredit Bangun Rumah
            </h1>
            <ul className="flex gap-3 items-center">
              <li>
                <Image src={Mandiri} alt="icon" />
              </li>
              <li>
                <Image src={Btn} alt="icon" />
              </li>
              <li>
                <Image src={Bni} alt="icon" />
              </li>
            </ul>
          </div>

          <div className="mb-8 md:mb-0">
            <h1 className="text-[#fff] font-semibold text-base">
              Tunai Via Bank Transfer
            </h1>
            <ul className="flex gap-3 items-center">
              <li>
                <Image src={Mandiri} alt="icon" />
              </li>
              <li>
                <Image src={Btn} alt="icon" />
              </li>
              <li>
                <Image src={Bni} alt="icon" />
              </li>
            </ul>
          </div>

          <div className="mb-8 md:mb-0">
            <h1 className="text-[#fff] font-semibold text-base">
              Kartu Kredit
            </h1>
            <ul className="flex gap-3 items-center">
              <li>
                <Image src={Visa} alt="icon" />
              </li>
              <li>
                <Image src={MasterCard} alt="icon" />
              </li>
              <li>
                <Image src={Jcb} alt="icon" />
              </li>
            </ul>
          </div>

          <div className="mb-8 md:mb-0">
            <h1 className="text-[#fff] font-semibold text-base">
              Rekan Teknologi Tambahan
            </h1>
            <ul className="flex gap-3 items-center">
              <li>
                <Image src={Asco} alt="icon" />
              </li>
              <li>
                <Image src={Ineco} alt="icon" />
              </li>
              <li>
                <Image src={Agra} alt="icon" />
              </li>
            </ul>
          </div>
        </div>

        <div className="footer-bottom block md:flex  gap-2 items-center justify-between">
          <h1 className="flex gap-2  text-[#fff] items-end text-base font-normal mb-5">
            Powered by: <Image src={Sig} alt="icon" />
          </h1>
          <p className="text-[#fff] text-md font-normal">
            Copyright © 2023 SobatBangun. All rights reserved.
          </p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
