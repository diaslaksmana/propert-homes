import Footer from "@/comnponent/Footer";
import Header from "@/comnponent/Header";
import Home from "./home/page";

export default function page() {
  return (
    <>
      <Header />
      <Home />
      <Footer />
    </>
  );
}
