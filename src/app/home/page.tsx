import Slider from "@/comnponent/Slider";
import React from "react";
import Image from "next/image";
import IconUser from "../../assets/Frame1261152622.png";
import IconVerived from "../../assets/Frame1261152327.png";
import DimensionIcon from "../../assets/material-symbols_width.png";
import LBIcon from "../../assets/mdi_house-swap-outline.png";
import Floor from "../../assets/ph_stairs.png";
import BedroomIcon from "../../assets/material-symbols_bedroom-parent-outline.png";
import pictProfile from "../../assets/Vector(5).png";
import stars from "../../assets/Vector(4).png";

const Home = () => {
  return (
    <div className="home mt-[150px]">
      <div className="container mx-auto px-5">
        {/* Breadcrumb */}
        <div className="breadcrumb">
          <ul className="flex items-center gap-4">
            <li>Home</li>
            <li>Layanan Desain</li>
            <li>Omah Apik 3</li>
          </ul>
        </div>
      </div>

      {/* Slider */}
      <div className="banner my-8">
        <Slider />
      </div>

      <div className="container mx-auto px-5">
        <div className="detail block lg:flex gap-10">
          <div className="detail-picture basis-4/6 mb-8">
            <h1 className="text-[24px] font-semibold mb-4">Tampilan Rumah</h1>
            <div className="grid grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-4">
              <div className="card border-[1px] border-[#E6E6E6] p-4 rounded-[8px]">
                <div className="card-header">
                  <Image
                    src="https://images.pexels.com/photos/1571458/pexels-photo-1571458.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                    alt="Image"
                    width={100}
                    height={100}
                    className="aspect-square rounded w-full object-cover object-center"
                  />
                </div>
                <div className="card-body mt-3">
                  <h1 className="text-xl font-[700] mb-4">Ruang Keluarga </h1>
                  <p className="text-sm font-[400]">2.0 x 2.9 </p>
                </div>
              </div>
              <div className="card border-[1px] border-[#E6E6E6] p-4 rounded-[8px]">
                <div className="card-header">
                  <Image
                    src="https://images.pexels.com/photos/1571458/pexels-photo-1571458.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                    alt="Image"
                    width={100}
                    height={100}
                    className="aspect-square rounded w-full object-cover object-center"
                  />
                </div>
                <div className="card-body mt-3">
                  <h1 className="text-xl font-[700] mb-4">Kamar Tidur </h1>
                  <p className="text-sm font-[400]">2.0 x 2.9 </p>
                </div>
              </div>
              <div className="card border-[1px] border-[#E6E6E6] p-4 rounded-[8px]">
                <div className="card-header">
                  <Image
                    src="https://images.pexels.com/photos/1571458/pexels-photo-1571458.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                    alt="Image"
                    width={100}
                    height={100}
                    className="aspect-square rounded w-full object-cover object-center"
                  />
                </div>
                <div className="card-body mt-3">
                  <h1 className="text-xl font-[700] mb-4">
                    Ruang Makan & Dapur{" "}
                  </h1>
                  <p className="text-sm font-[400]">2.0 x 2.9 </p>
                </div>
              </div>
              <div className="card border-[1px] border-[#E6E6E6] p-4 rounded-[8px]">
                <div className="card-header">
                  <Image
                    src="https://images.pexels.com/photos/1571458/pexels-photo-1571458.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                    alt="Image"
                    width={100}
                    height={100}
                    className="aspect-square rounded w-full object-cover object-center"
                  />
                </div>
                <div className="card-body mt-3">
                  <h1 className="text-xl font-[700] mb-4">Ruang Kerja </h1>
                  <p className="text-sm font-[400]">2.0 x 2.9 </p>
                </div>
              </div>
              <div className="card border-[1px] border-[#E6E6E6] p-4 rounded-[8px]">
                <div className="card-header">
                  <Image
                    src="https://images.pexels.com/photos/1571458/pexels-photo-1571458.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                    alt="Image"
                    width={100}
                    height={100}
                    className="aspect-square rounded w-full object-cover object-center"
                  />
                </div>
                <div className="card-body mt-3">
                  <h1 className="text-xl font-[700] mb-4">Kamar Tidur </h1>
                  <p className="text-sm font-[400]">2.0 x 2.9 </p>
                </div>
              </div>
            </div>
          </div>
          <div className="overview basis-2/6">
            <div className=" border-[1px] border-[#E6E6E6] p-4 rounded-[8px]">
              <h1 className="text-[24px] font-[600] mb-4">Omah Apik 3</h1>
              <h1 className="flex items-center gap-4 ">
                <Image
                  src={IconUser}
                  alt="IconUser"
                  className="h-[28px] w-[28px] rounded-full"
                />
                <span className="text-sm font-normal">Studio SAe</span>
              </h1>
              <ul className="flex gap-5 py-5 border-b-[1px] border-[#E6E6E6] ">
                <li className="">
                  <h1 className="mb-3 text-sm font-normal text-[#4D4D4D]">
                    Jenis Rumah
                  </h1>
                  <p className="text-sm font-normal text-[#4D4D4D]">
                    Tipe Desain
                  </p>
                </li>
                <li className="">
                  <h1 className="mb-3 text-sm font-normal text-[#000]">
                    Scandinavian
                  </h1>
                  <p className="flex items-center gap-3">
                    <Image
                      src={IconVerived}
                      alt="icon user"
                      className="w-[16px] h-[16px]"
                    />
                    <span className="text-sm font-normal text-[#F5333F]">
                      Dapat Dimodifikasi
                    </span>
                  </p>
                </li>
              </ul>
              <ul className="flex items-center justify-between border-b-[1px] py-3 border-[#E6E6E6]">
                <li>
                  <div className="card flex justify-between flex-col items-center">
                    <Image
                      src={DimensionIcon}
                      alt="icon user"
                      className="my-4"
                    />
                    <h1 className="text-[#808080] text-xs font-normal">
                      Dimensi Tanah
                    </h1>
                    <p className="text-[#000] text-sm font-normal">15 x 8m</p>
                  </div>
                </li>
                <li>
                  <div className="card flex justify-between flex-col items-center">
                    <Image src={LBIcon} alt="icon user" className="my-4" />
                    <h1 className="text-[#808080] text-xs font-normal">
                      Luas Bangunan
                    </h1>
                    <p className="text-[#000] text-sm font-normal">112m2</p>
                  </div>
                </li>
                <li>
                  <div className="card flex justify-between flex-col items-center">
                    <Image src={Floor} alt="icon user" className="my-4" />
                    <h1 className="text-[#808080] text-xs font-normal">
                      Lantai
                    </h1>
                    <p className="text-[#000] text-sm font-normal">2</p>
                  </div>
                </li>
                <li>
                  <div className="card flex justify-between flex-col items-center">
                    <Image src={BedroomIcon} alt="icon user" className="my-4" />
                    <h1 className="text-[#808080] text-xs font-normal">
                      Kamar Tidur
                    </h1>
                    <p className="text-[#000] text-sm font-normal">4</p>
                  </div>
                </li>
              </ul>
              <div className="my-4">
                <h1 className="text-xs font-normal">Harga Desain</h1>
                <h1 className="text-[32px] font-semibold">Rp. 32.500.000</h1>
                <p className="text-[#808080] text-sm font-normal mb-4">
                  Harga konstruksi mulai dari Rp 560.000.000
                </p>
                <a
                  href=""
                  className="btn-primary h-[48px] grid place-content-center w-full  text-center"
                >
                  Konsultasi Sekarang
                </a>
              </div>
            </div>
            <div className="testimoni my-5">
              <h1 className="text-2xl font-semibold mb-5">Testimoni</h1>
              <ul className="flex flex-col gap-5">
                {[1, 2, 3, 4, 5].map((item: any) => (
                  <li key={item}>
                    <div className="card flex gap-5 ">
                      <Image
                        src={pictProfile}
                        alt="pictProfile"
                        className="h-[36.67px] w-[36.67px] rounded-full"
                      />
                      <div className="comment">
                        <h1 className="flex gap-2 items-center">
                          <span className="text-base font-semibold">
                            Budi Setiadi
                          </span>
                          |
                          <span className="text-[#4A5568] flex items-center gap-2">
                            <Image
                              src={stars}
                              alt="stars"
                              className="h-[13.14px] w-[13.14px]"
                            />
                            4.5
                          </span>
                        </h1>
                        <p className="text-[#666666] font-normal text-base">
                          Desainnya sangat bagus dan pengirimannya cepat. Terima
                          kasih Sobat Bangun
                        </p>
                      </div>
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>

        <div className="other-desain mt-[100px]">
          <h1 className="text-2xl font-semibold mb-4">
            Desain Lainnya oleh Studio SAe
          </h1>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4  gap-4">
            {[1, 2, 3, 4].map((item) => (
              <div
                className="card border-[1px] border-[#E6E6E6] p-4 rounded-[8px]"
                key={item}
              >
                <div className="card-header">
                  <Image
                    src="https://images.pexels.com/photos/276724/pexels-photo-276724.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                    alt="Image"
                    width={100}
                    height={100}
                    className=" aspect-[4/3] rounded w-full object-cover object-center"
                  />
                </div>
                <div className="card-body mt-3">
                  <h1 className="text-xl font-[700] flex items-center gap-3 mb-4">
                    Omah Apik 1
                    <span className="bg-[#F1F1F1] py-2 px-4 text-sm rounded-[16px] font-normal">
                      Scandinavian
                    </span>
                  </h1>
                  <h1 className="flex items-center gap-4">
                    <Image
                      src={IconUser}
                      alt="icon user"
                      className="h-[28px] w-[28px] rounded-full"
                    />
                    <span className="text-sm font-normal">Studio SAe</span>
                  </h1>
                  <ul className=" flex items-center justify-between border-b-[1px] py-3 border-[#E6E6E6]">
                    <li>
                      <div className="card flex justify-between flex-col items-center">
                        <div className="h-[34px] w-[24px] grid place-content-center">
                          <Image
                            src={DimensionIcon}
                            alt="icon user"
                            className="my-4 max-h-[24px] max-w-[24px] "
                          />
                        </div>
                        <h1 className="text-[#808080] min-h-[30px] grid place-content-center text-center text-xs font-normal">
                          Dimensi Tanah
                        </h1>
                        <p className="text-[#000] text-sm font-normal">
                          15 x 8m
                        </p>
                      </div>
                    </li>
                    <li>
                      <div className="card flex justify-between flex-col items-center">
                        <div className="h-[34px] w-[24px] grid place-content-center">
                          <Image
                            src={LBIcon}
                            alt="icon user"
                            className="my-4  max-h-[24px] max-w-[24px]"
                          />
                        </div>

                        <h1 className="text-[#808080] min-h-[30px] grid place-content-center text-center text-xs font-normal">
                          Luas Bangunan
                        </h1>
                        <p className="text-[#000] text-sm font-normal">112m2</p>
                      </div>
                    </li>
                    <li>
                      <div className="card flex justify-between flex-col items-center">
                        <div className="h-[34px] w-[24px] grid place-content-center">
                          <Image
                            src={Floor}
                            alt="icon user"
                            className="my-4  max-h-[24px] max-w-[24px]"
                          />
                        </div>

                        <h1 className="text-[#808080] min-h-[30px] grid place-content-center text-center text-xs font-normal">
                          Lantai
                        </h1>
                        <p className="text-[#000] text-sm font-normal">2</p>
                      </div>
                    </li>
                    <li>
                      <div className="card flex justify-between flex-col items-center">
                        <div className="h-[34px] w-[24px] grid place-content-center">
                          <Image
                            src={BedroomIcon}
                            alt="icon user"
                            className="my-4  max-h-[24px] max-w-[24px]"
                          />
                        </div>

                        <h1 className="text-[#808080] min-h-[30px] grid place-content-center text-center text-xs font-normal">
                          Kamar Tidur
                        </h1>
                        <p className="text-[#000] text-sm font-normal">4</p>
                      </div>
                    </li>
                  </ul>
                  <div className="my-4">
                    <h1 className="text-xs font-normal">Harga Desain</h1>
                    <h1 className="text-[32px] font-semibold">
                      Rp. 32.500.000
                    </h1>
                    <p className="text-[#808080] text-sm font-normal mb-4">
                      Harga konstruksi mulai dari Rp 560.000.000
                    </p>
                    <a href="" className="btn-primary-outline">
                      Lihat Detail
                    </a>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
